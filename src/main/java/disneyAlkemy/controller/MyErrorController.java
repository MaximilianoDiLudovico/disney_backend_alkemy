package disneyAlkemy.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class MyErrorController implements disneyAlkemy.controller.ErrorController {

    @RequestMapping("/error")
    public String handleError() {
        log.info("Error.No se ha podido acceder a la direccion solicitada");
        return "Error. No se ha podido acceder a la direccion solicitada." +
                "Intente con http://localhost:8080/login." +
                "Usuario :backendalkemy@gmail.com" +
                "Password:@lkemy123";
    }

    @Override
    public String getErrorPath() {
        return null;
    }
}
