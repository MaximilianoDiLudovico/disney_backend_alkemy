package disneyAlkemy.services;

import java.io.Serializable;
import java.util.Optional;

import disneyAlkemy.model.Movies;
import disneyAlkemy.repository.MoviesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MoviesServices implements Serializable {

    private static final long serialVersionUID = 1L;
    @Autowired
    private MoviesRepository movieRepository;

    @Transactional(readOnly = true)
    public Iterable<Movies> findAll(){
        return movieRepository.findAll();
    }
    @Transactional(readOnly = true)
    public Iterable<Object[]> getAll(){
        return movieRepository.getAll();
    }

    @Transactional(readOnly = true)
    public Optional<Movies> findById(Integer id){
        return movieRepository.findById(id);
    }
    @Transactional
    public Movies save(Movies movie)
    {
        return movieRepository.save(movie);
    }
    @Transactional(readOnly = true)
    public Iterable<Object[]> findByTitle(String title){
        return movieRepository.findByTitle(title);
    }
    @Transactional(readOnly = true)
    public Iterable<Object[]> getByOrder(String order){
        if(order.equals("ASC")){
            return movieRepository.getAllByOrderASC();            
        }else if(order.equals("DESC")){
            return movieRepository.getAllByOrderDESC();  
        }else{
            return movieRepository.getAll();
        }
    }
    @Transactional
    public boolean delete(Integer id){
        try{
            movieRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
}
