package disneyAlkemy.services;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import disneyAlkemy.model.Genders;
import disneyAlkemy.model.Movies;
import disneyAlkemy.repository.GendersRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GendersServices implements Serializable {

    private static final long serialVersionUID = 1L;
    @Autowired
    private GendersRepository gendersRepository;

    @Transactional
    public Genders save(Genders gender){
        return gendersRepository.save(gender);       
    }
    public boolean delete(Integer id){
        try{
            gendersRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
        
    }
    @Transactional(readOnly = true)
    public Optional<Genders> findById(Integer genderId) {
        return gendersRepository.findById(genderId);
    }
    @Transactional(readOnly = true)
    public List<Movies> getMoviesByGenreId(Integer genreId) {
        Genders gender = gendersRepository.getById(genreId);
        if(gender != null){
            List<Movies> movies = gender.getMoviesID();   
            return movies;
        }else{
            return null;
        }
    }
}