package disneyAlkemy.services;

import java.io.Serializable;
import java.util.Optional;

import disneyAlkemy.model.Characters;
import disneyAlkemy.repository.CharacterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CharacterServices implements Serializable {

    private static final long serialVersionUID = 1L;
    @Autowired
    private CharacterRepository characterRepository;
    
    public Iterable<Characters> findAll(){
        return characterRepository.findAll();
    }
    public Iterable<Object[]> getAll() {
        return characterRepository.getAll();
    }
    public Optional<Characters> findById(Integer characterId) {
        return characterRepository.findById(characterId);
    }
    public Iterable<Object[]> findByName(String name) {
        return characterRepository.findByName(name);
    }
    public Iterable<Object[]> findByAge(Integer age) {
        return characterRepository.findByAge(age);
    }

    @Transactional
    public boolean delete(Integer id){
        try{
            characterRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
        
    }
    @Transactional
    public Characters save(Characters character) {
        return characterRepository.save(character);
    }
    @Transactional
    public Characters putSave(Characters character) {
        return characterRepository.save(character);
    }

}
