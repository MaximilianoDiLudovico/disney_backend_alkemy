package disneyAlkemy.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "characters")

/**
 * Clase que contiene los atributos solicitados del personaje
 */
public class Characters {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer idCharacter;
        private String name;
        private String image;
        private Integer age;
        private Integer weight;
        private String history;
        @JsonManagedReference
        @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        @JoinTable(name = "characters_movies",
        joinColumns = {
                @JoinColumn(name = "idCharacter", nullable = false)},
        inverseJoinColumns = {
                @JoinColumn(name = "idMovie", nullable = false)})
        private List<Movies> filmsID;
}