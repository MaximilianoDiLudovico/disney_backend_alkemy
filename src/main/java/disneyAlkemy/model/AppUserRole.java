package disneyAlkemy.model;

public enum AppUserRole {
    USER,
    ADMIN
}
