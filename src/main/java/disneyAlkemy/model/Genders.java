package disneyAlkemy.model;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Data
@Entity
@Table(name = "genders")

/**
 *  Clase que contiene los atributos solicitados del genero
 */
public class Genders {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idGender;
    private String name;
    private String image;

    @JsonManagedReference
    @OneToMany(mappedBy = "gendersID",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Movies> moviesID;

}